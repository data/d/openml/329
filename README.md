# OpenML dataset: hayes-roth

https://www.openml.org/d/329

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Barbara and Frederick Hayes-Roth  
  
**Source**: [original](https://archive.ics.uci.edu/ml/datasets/Hayes-Roth) -   
**Please cite**:   

Hayes-Roth Database

This is a merged version of the separate train and test set which are usually distributed. On OpenML this train-test split can be found as one of the possible tasks.

Source Information: 
(a) Creators: Barbara and Frederick Hayes-Roth 
(b) Donor: David W. Aha (aha@ics.uci.edu) (714) 856-8779  
(c) Date: March, 1989  

Attribute Information: 
-- 1. name: distinct for each instance and represented numerically 
-- 2. hobby: nominal values ranging between 1 and 3 
-- 3. age: nominal values ranging between 1 and 4 
-- 4. educational level: nominal values ranging between 1 and 4 
-- 5. marital status: nominal values ranging between 1 and 4 
-- 6. class: nominal value between 1 and 3  

Detailed description of the experiment: 
1. 3 categories (1, 2, and neither -- which I call 3) 
-- some of the instances could be classified in either class 1 or 2, and they have been evenly distributed between the two classes 
2. 5 Attributes 
-- A. name (a randomly-generated number between 1 and 132) 
-- B. hobby (a randomly-generated number between 1 and 3) 
-- C. age (a number between 1 and 4) 
-- D. education level (a number between 1 and 4) 
-- E. marital status (a number between 1 and 4) 
3. Classification:  
-- only attributes C-E are diagnostic; values for A and B are ignored 
-- Class Neither: if a 4 occurs for any attribute C-E 
-- Class 1: Otherwise, if (# of 1's)>(# of 2's) for attributes C-E 
-- Class 2: Otherwise, if (# of 2's)>(# of 1's) for attributes C-E 
-- Either 1 or 2: Otherwise, if (# of 2's)=(# of 1's) for attributes C-E 
4. Prototypes: 
-- Class 1: 111 
-- Class 2: 222 
-- Class Either: 333 
-- Class Neither: 444

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/329) of an [OpenML dataset](https://www.openml.org/d/329). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/329/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/329/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/329/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

